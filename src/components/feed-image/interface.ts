export interface FeedImageProps {
  src: string;
  alt?: string;
}
