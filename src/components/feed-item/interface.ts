export interface FeedPicture {
  large: string;
  medium: string;
  thumbnail: string;
}
export interface FeedName {
  first: string;
  last: string;
  title: string;
}
export interface FeedItem {
  email: string;
  name: FeedName;
  picture: FeedPicture;
  id?: number;
}
export interface FeedItemProps {
  item: FeedItem;
}
