import type { Ref } from 'vue';
import type { FeedItem } from '@/components/feed-item/interface';
export interface Query {
  page: number;
  results: number;
}
export interface FeedState {
  feed: Ref<FeedItem[]>;
  getFeed(): Promise<void>;
  alt(item: FeedItem): string;
  blank(): FeedItem;
}
export interface FeedResponse {
  results: FeedItem[];
}
