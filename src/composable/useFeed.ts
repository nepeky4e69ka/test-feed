import { ref } from 'vue';
import { api } from '@/services/api';
import { config } from '@/utils';
import type { FeedItem } from '@/components/feed-item/interface';
import type { FeedState, FeedResponse } from './useFeed.interface';
/**
 * @type {FeedState}
 * @description feed state
 */
export function useFeed(): FeedState {
  const loading = ref(false);
  const feed = ref<FeedItem[]>([]);
  const query = ref({
    page: 0,
    results: config.MAX_ITEMS,
  });
  /**
   *
   * @function getFeed increment page & load new feed items
   */
  async function getFeed(): Promise<void> {
    if (loading.value) {
      return;
    }
    query.value.page++;
    loading.value = true;
    try {
      /**
       * @type {FeedResponse}
       * @description Raw data from api & push the required fields
       */
      const { results } = await api.get<FeedResponse>(query.value);
      results.map(({ name, email, picture }) =>
        feed.value.push({
          name,
          email,
          picture,
        }),
      );
    } catch (e) {
      console.trace(e);
    }
    loading.value = false;
  }
  /**
   *
   * @returns { FeedItem } with empty data
   */
  function blank(): FeedItem {
    return {
      email: '',
      picture: {
        large: '',
        medium: '',
        thumbnail: '',
      },
      name: {
        first: '',
        last: '',
        title: '',
      },
    };
  }
  function alt(item: FeedItem): string {
    return `${item.name.first} ${item.name.first}`;
  }

  return { feed, getFeed, alt, blank };
}
