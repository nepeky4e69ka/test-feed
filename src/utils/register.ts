import type { App } from 'vue';
import { lazyImage, itemBlank, itemVisible } from './directives';
import { DirectiveName as DN, ComponentName as CN } from './config';
import FeedList from '@/components/feed-list/component.vue';
import FeedImage from '@/components/feed-image/component.vue';
import FeedItem from '@/components/feed-item/component.vue';
import FeedItemBlank from '@/components/feed-item-blank/component.vue';
/**
 *
 * @param app
 * @returns app
 * @description register directives && components
 */
export const register = (app: App): App => {
  // Directives
  app.directive(DN.LAZY_IMAGE, lazyImage);
  app.directive(DN.ITEM_VISIBLE, itemVisible);
  app.directive(DN.ITEM_BLANK, itemBlank);
  // Components
  app.component(CN.FEED_ITEM, FeedItem);
  app.component(CN.FEED_ITEM_BLANK, FeedItemBlank);
  app.component(CN.FEED_IMAGE, FeedImage);
  app.component(CN.FEED_LIST, FeedList);
  return app;
};
