/**
 * @constant USER_API url provided from .env
 * @constant MAX_ITEMS max load feed items
 */
export const config = {
  USER_API: String(import.meta.env.VITE_USER_API) || '',
  MAX_ITEMS: 16,
};

export enum EventName {
  GET_FEED = 'getFeed',
}

export enum DirectiveName {
  LAZY_IMAGE = 'lazy-image',
  ITEM_VISIBLE = 'item-visible',
  ITEM_BLANK = 'item-blank',
}

export enum ComponentName {
  FEED_ITEM = 'FeedItem',
  FEED_ITEM_BLANK = 'FeedItemBlank',
  FEED_IMAGE = 'FeedImage',
  FEED_LIST = 'FeedList',
}
