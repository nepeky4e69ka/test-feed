import type { Directive } from 'vue';
import { EventName } from './config';
/**
 * @implements directive for lazy-load image
 */
const lazyImage: Directive = {
  mounted(el) {
    try {
      const handler = (entries: IntersectionObserverEntry[]): void => {
        const { isIntersecting, target } = entries[0];
        if (isIntersecting) {
          const image = target as HTMLImageElement;
          if (image.dataset.src) {
            image.src = image.dataset.src;
            observer.unobserve(el);
          }
        }
      };
      const observer = new IntersectionObserver(handler);
      observer.observe(el);
    } catch (e) {
      void e;
    }
  },
};
/**
 * @implements directive hides the item when they are out of the observation area
 */
// PS
// по мне лучше убирать элементы из DOM, а те что есть с помощью translate с position absolute двигать за прокруткой
const itemVisible: Directive = {
  mounted(el) {
    try {
      const handler = (entries: IntersectionObserverEntry[]): void => {
        const { isIntersecting, target } = entries[0];
        const item = target as HTMLImageElement;
        if (isIntersecting) {
          item.classList.remove('-hide');
        } else {
          item.classList.add('-hide');
        }
      };
      const observer = new IntersectionObserver(handler);
      observer.observe(el);
    } catch (e) {
      void e;
    }
  },
};
/**
 * @implements directive for trigger event
 */
const itemBlank: Directive = {
  mounted(el, vm) {
    try {
      const handler = (entries: IntersectionObserverEntry[]): void => {
        const { isIntersecting } = entries[0];
        if (isIntersecting) {
          /**
           * @emits getFeed event triggered and loads more data
           */
          vm.instance?.$emit(EventName.GET_FEED);
        }
      };
      const observer = new IntersectionObserver(handler);
      observer.observe(el);
    } catch (e) {
      void e;
    }
  },
};
export { lazyImage, itemBlank, itemVisible };
