/**
 * @type { Base } to avoid number issue
 */
export type Base = Record<string, string>;
export type Params = Record<string, string | number | boolean>;
