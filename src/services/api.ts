import { config } from '@/utils';
import { Base, Params } from './api.interface';
/**
 * @class for create Api instance
 */
class Api {
  private baseUrl = config.USER_API;
  private async req<T>(url: string): Promise<T> {
    const response = await fetch(url);
    return await response.json();
  }
  public get<T>(params: Params): Promise<T> {
    const qs = this.queryToString(params);
    const url = `${this.baseUrl}${qs}`;
    return this.req<T>(url);
  }
  private queryToString(query: Params): string {
    const params = new URLSearchParams(query as Base);
    return `?${params.toString()}`;
  }
}
const api = new Api();
export { api };
